# Generated by Django 4.2 on 2023-07-14 22:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restapp', '0013_user_is_active'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='is_active',
        ),
    ]
