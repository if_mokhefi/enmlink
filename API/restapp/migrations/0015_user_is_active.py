# Generated by Django 4.2 on 2023-07-14 22:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('restapp', '0014_remove_user_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
    ]
